let age;
let name;
let answer;

age = parseInt(prompt("Enter your age", age));
name = prompt("Enter your name", name);

if (isNaN(age)) {
    do {
        if (isNaN(age))
            age = parseInt(prompt("Age was entered incorrectly!"));

    } while (isNaN(age))
}
if (name.trim() == "") {
    do {
        name = prompt("Name was not entered!", name);
    } while (name.trim() == "")
}
if (age > 22)
    alert("Welcome " + name);
else if (age >= 18) {
    answer = confirm("Are you sure wou want to continue?");
    if (answer)
        alert("Welcome " + name);
    else
        alert("You are not allowed to visit this website")
} else
    alert("You are not allowed to visit this website");

//Область видимости var переменных ограничена функцией, const и let ограничены блоком, разница между let и const в том, что в let меняется значение переменной, а в const нет.
// С появлением ES2015 потребность в var отпала.

